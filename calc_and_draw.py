#if i add sth here will it be displayed on sourcetree?
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math

def main():
	dt = 0.01#time step
	pe = 1.8
	
	for i in range(100,2000,100):#start,end,step
		ta1 = cal(i,dt,pe)
		tadf = pd.DataFrame(ta1)
		tadf = tadf.T
		#tadf.to_csv("testfordraw_%02d.csv",index = False)#available if need .csv files
		draw(tadf,i)
	return 0

def cal(n,dt,pe):	
	dx = 0.5#kousi-size on x-direction
	dy = 0.5
	u = 1.0#speed on x-direction
	v = 0
	ta = np.array([[0.0]*20 for i in range(20)])#nombre of x-directed-kousi
	tb = np.array([[0.0]*20 for i in range(20)])
	ta[8:11,8:11] = 1#center set to be always 1
	for k in range(n):#do n times calculate
		for i in range(19):#for each line
			for j in range(19):#for each line
				tb[i][j] = ta[i][j]
				c=(ta[i+1][j]-2.0*ta[i][j]+ta[i-1][j])/(dx*dx)
				d=(ta[i][j+1]-2.0*ta[i][j]+ta[i][j-1])/(dy*dy)
				a=(ta[i+1][j]-ta[i-1][j])/2*dx
				b=(ta[i][j+1]-ta[i][j-1])/2*dy
				ta[i][j] = ta[i][j] + dt * ((1/pe)*(c+d)-u*(a-abs(dx/2)*c)-v*(b-abs(dy/2)*d))#with visc or not? 
				ta[8:11,8:11] = 1
	return ta
	
def draw(df,i):
	fig,ax = plt.subplots()#draw a blanc figure with x,y axes
	fig.suptitle('temperature in the pool', fontsize=14, fontweight='bold')
	heatmap = ax.pcolor(df,cmap = plt.cm.hot)#generate a heatmap
	ax.set_xticks(np.arange(20),minor = False)
	ax.set_yticks(np.arange(20),minor = False)#set x and y axis to be formed as 0,1,2,3,...
	ax.set_xlabel('x')
	ax.set_ylabel('y')
	ax.invert_yaxis()#invert the direction of y axis
	ax.xaxis.tick_top()#turn x axis to the top of figure
	plt.savefig('figure%02.f'%i+'.png')#to save the figure, unavailable the show().
	
	
if __name__ == '__main__':
    main()